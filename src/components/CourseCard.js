import {useState, useEffect} from 'react'
import {Button, Card} from 'react-bootstrap';
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'

export default function CourseCard({course}) {

  // Destructuring the props
  const {name, description, price, _id} = course

  // Initialize a 'count' state with a value of zero (0)
  const [count, setCount] = useState(0)
  const [slots, setSlots] = useState(15)
  const [btnStatus, setBtnStatus] = useState(false)
  const [isOpen, setIsOpen] = useState(true)

  // function enroll(){
  //   if(slots === 0){
  //     alert("There is no slots available")
  //     setBtnStatus(true)
  //     return
  //   }

  //   setCount(count + 1)
  //   setSlots(slots - 1)
  // }

  // Effects in React is just like side effects/effects in real life, where everytime something happens within the component, a function or condition runs.
  // You may also listen or watch a specific state for changes instead of watching/listening to the whole component
  // If useEffect is blank [], it will listen to all the components (can also be single or multiple)
  // useEffect(() => {
  //   if(slots === 0){
  //     setIsOpen(false)
  //   }
  // }, [slots])

  return (
    <Card style={{ width: "100%" }}>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>
          {description}
        </Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>
          PHP {price}
        </Card.Text>
        <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
      </Card.Body>
    </Card>
  );
}

// Prop Types can be used to validate the data coming from the props. You can define each property of the prop and assign specific validation for each of them. 
CourseCard.propTypes = {
  course: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  })
}

